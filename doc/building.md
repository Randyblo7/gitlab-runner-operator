# Building and pushing all images

1. Make sure that the multiarch images and manifests are built for [gitlab-runner-ocp](https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images/container_registry/1766421) and [gitlab-runner-helper-ocp](https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images/container_registry/1766433). E.g. `registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images/gitlab-runner-ocp:v14.7.0`, `registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images/gitlab-runner-ocp:amd64-v14.7.0` and `registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images/gitlab-runner-ocp:ppc64le-v14.7.0`.

1. Export the relevant variables:

    ```shell
    export VERSION=1.6.0
    export CERTIFIED=false
    ```

1. Build the Operator images:

    ```shell
    PLATFORM=linux/amd64 make build-and-push-operator-image
    PLATFORM=linux/ppc64le make build-and-push-operator-image
    ```

1. Build the bundle and bundle image:

    ```shell
    make build-and-push-bundle-image
    ```

1. Build manifests:

    ```shell
    PLATFORMS="linux/amd64,linux/ppc64le" make manifest-operator-image
    ```

1. To build a catalog source for [local testing](https://docs.gitlab.com/runner/install/openshift.html#install-other-versions-of-gitlab-runner):

    ```shell
    PLATFORM=linux/amd64 make build-and-push-catalog-source
    PLATFORM=linux/ppc64le make build-and-push-catalog-source
    PLATFORMS="linux/amd64,linux/ppc64le" make build-and-push-catalog-source-manifest
    ```