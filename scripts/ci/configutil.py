import config
import dockerutil
import image
import os
from pathlib import Path
from ruamel import yaml


def write_release_config():
    RELEASE_CONFIG_PATH = "hack/assets/release.yaml"
    Path(os.path.dirname(RELEASE_CONFIG_PATH)).mkdir(
        parents=True, exist_ok=True)
    print(f"Creating release config at {RELEASE_CONFIG_PATH}")

    with open(RELEASE_CONFIG_PATH, "w") as f:
        release_config = yaml.safe_load(
            '''
            version: {operator_version}
            images:
                - name: gitlab-runner
                  upstream: {runner_image}
                - name: gitlab-runner-helper
                  upstream: {runner_helper_image}
            '''.format(**{
                "operator_version": config.operator_version(),
                "runner_image": dockerutil.image_with_digest(image.IMAGE_NAME_RUNNER, config.arch()),
                "runner_helper_image": dockerutil.image_with_digest(image.IMAGE_NAME_HELPER, config.arch()),
            })
        )

        print(f"Release config: \n{release_config}")

        yaml.dump(release_config, f)


def write_related_images():
    RELATED_IMAGES_CONFIG_PATH = "bundle/patches/related_images.yaml"
    Path(os.path.dirname(RELATED_IMAGES_CONFIG_PATH)).mkdir(
        parents=True, exist_ok=True)

    print(f"Creating related images CSV at {RELATED_IMAGES_CONFIG_PATH}")

    with open(RELATED_IMAGES_CONFIG_PATH, "w") as f:
        related_images = yaml.safe_load(
            '''
            apiVersion: operators.coreos.com/v1alpha1
            kind: ClusterServiceVersion
            metadata:
              name: gitlab-runner-operator:{operator_version}
            spec:
              relatedImages:
              - name: gitlab-runner
                image: {runner_image}
              - name: gitlab-runner-helper
                image: {helper_image}
              - name: gitlab-runner-operator
                image: {operator_image}
              - name: kube-rbac-proxy
                image: {kube_rbac_proxy_image}
            '''.format(**{
                "operator_version": config.operator_version(clean=True),
                "runner_image": dockerutil.image_with_digest(image.IMAGE_NAME_RUNNER),
                "helper_image": dockerutil.image_with_digest(image.IMAGE_NAME_HELPER),
                "operator_image": dockerutil.image_with_digest(image.IMAGE_NAME_OPERATOR),
                "kube_rbac_proxy_image": config.kube_rbac_proxy_image(),
            })
        )

        print(f"Related images: \n{related_images}")

        yaml.dump(related_images, f)
