import config

IMAGE_NAME_RUNNER = "gitlab-runner-ocp"
IMAGE_NAME_HELPER = "gitlab-runner-helper-ocp"
IMAGE_NAME_OPERATOR = "gitlab-runner-operator"
IMAGE_NAME_BUNDLE = "gitlab-runner-operator-bundle"
IMAGE_NAME_CATALOG = "gitlab-runner-operator-catalog-source"

resources_mapping = {
    "operator": IMAGE_NAME_OPERATOR,
    "catalog": IMAGE_NAME_CATALOG,
}


def resource_to_image(resource):
    if not resource in resources_mapping:
        raise Exception("Unknown resource: {}".format(resource))

    return resources_mapping[resource]


def get_arch(name, arch):
    if name == IMAGE_NAME_HELPER and arch == "amd64":
        arch = "x86_64"

    return arch


def get(name, arch=None, version=None):
    arch = get_arch(name, arch)

    if name == IMAGE_NAME_RUNNER or name == IMAGE_NAME_HELPER:
        version = config.runner_revision() if not version else version
        return build(config.upstream_ubi_images_repository(), name, arch, version)
    elif name == IMAGE_NAME_OPERATOR or name == IMAGE_NAME_BUNDLE or name == IMAGE_NAME_CATALOG:
        version = config.operator_version() if not version else version
        return build(config.upstream_operator_images_repository(), name, arch, version)
    else:
        raise Exception("Unknown image name: {}".format(name))


def build(namespace, name, arch, version):
    if arch:
        return "{namespace}/{name}:{arch}-{version}".format(**{
            "namespace": namespace,
            "name": name,
            "arch": arch,
            "version": version,
        })

    return "{namespace}/{name}:{version}".format(**{
        "namespace": namespace,
        "name": name,
        "version": version,
    })


def manifest(manifest):
    platforms = config.platforms()
    image, version = _split_manifest_and_tag(manifest)
    arch_images = map(lambda platform: _map_arch_image(
        platform, image, version), platforms)

    return manifest, [i for i in arch_images]


def _split_manifest_and_tag(manifest):
    split = manifest.split(":")
    return split[0], split[1]


def _map_arch_image(platform, image, version):
    arch = get_arch(image, platform.split("/")[1])
    return "{}:{}-{}".format(image, arch, version)
